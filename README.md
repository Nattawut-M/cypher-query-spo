# Cypher Query

1. query :Disease -> :`Gene/Protein` -> :Exposure

> with parent-child relationships

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o:Exposure)
where  o.name in [
    'tobacco tar', 'Tobacco Smoke Pollution',
    'Carbon Monoxide',
    'Smoke',
    'allyl alcohol',
    'Vitamin D',
    'Glycerol'
 ]
return path
```

> without parent-child relationships

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o:Exposure)
where  
    NONE(r in r2 WHERE type(r)="parent-child") AND
    o.name in [
    'tobacco tar', 'Tobacco Smoke Pollution',
    'Carbon Monoxide',
    'Smoke',
    'allyl alcohol',
    'Vitamin D',
    'Glycerol'
 ]
return path
```

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o)
where  
    (o:`Effect/Phenotype` or o:Disease or o:Exposure) AND
    NONE(r in r2 WHERE type(r)="parent-child") AND
    o.name in [
    'tobacco tar', 'Tobacco Smoke Pollution',
    'Carbon Monoxide',
    'Smoke',
    'allyl alcohol',
    'Vitamin D',
    'Glycerol',
    'human papillomavirus-related squamous cell carcinoma'
 ]
return path
```

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o)
where 
    (o:`Effect/Phenotype` or o:Exposure or o:Disease) 
    and (o.name in ['human papillomavirus-related squamous cell carcinoma', 'human papillomavirus-related penile squamous cell carcinoma', 'human papilloma virus infection', 'persistent human papillomavirus infection'])
return path
```

No. 5

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o)
where 
    (o:`Effect/Phenotype` or o:`Gene/Protein` or o:Exposure or o:Disease) and
    NONE( rel in r2 WHERE type(rel) in ['parent-child'])
    and (o.name in ['human papillomavirus-related squamous cell carcinoma', 'human papillomavirus-related penile squamous cell carcinoma', 'human papilloma virus infection', 'persistent human papillomavirus infection'])
return path
```

No. 6 - 7

```c
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o)
where 
    (o:Exposure or o:Disease) and
    NONE( rel in r2 WHERE type(rel) in ['parent-child']) and
    (o.name in ['keratosis, familial actinic', 'keratosis', 'mucositis'])
return path
```

> all risk factor

```cypher
match path=(n:Disease {name: "oral cavity cancer"})-[r1*..2]->(m:`Gene/Protein`)-[r2*..2]->(o)
where 
    (o:Exposure or o:Disease) and
    NONE( rel in r2 WHERE type(rel) in ['parent-child']) and
    (o.name in [
    'keratosis, familial actinic', 'keratosis', 
    'mucositis', 
    'tobacco tar', 'tobacco smoke pollution',
    'carbon monoxide',
    'smoke',
    'allyl alcohol',
    'vitamin d',
    'glycerol',
    'margarine',
    'tea'
])
return path
```

ChatGPT

```cypher
MATCH path=allShortestPaths((n:Disease {name: "oral cavity cancer"})-[r1*..4]->(o))
WHERE 
    (o:`Gene/Protein` or o:Exposure or o:Disease) and
    NONE( rel in r1 WHERE type(rel) in ['parent-child']) and
    (o.name in [
    'keratosis, familial actinic', 'keratosis', 
    'mucositis', 
    'tobacco tar', 'Tobacco Smoke Pollution',
    'Carbon Monoxide',
    'Smoke',
    'allyl alcohol',
    'Vitamin D',
    'Glycerol',
    'Margarine'
])
RETURN path
```

<!-- original risk factors by PM -->
WITH [
    'tobacco tar', 'tobacco smoke pollution',
    "Exacerbated by tobacco use",

    'smoke',
    'ethanol',

    "anogenital human papillomavirus infection",
    'human papilloma virus infection',

    'human herpesvirus 8-related tumor',
    'allyl alcohol',
    
    'herpes simplex virus keratitis',
    'syphilis',
    'candida glabrata',
    'periodontitis',
    'mucocutaneous ulceration, chronic',
    'bacterial infectious disease with sepsis',
    'Formaldehyde',
    'Sulfur Dioxide',
    'Asbestos',
    'Pesticides',
    'malignant tumor of floor of mouth',
    'tp53',
    'GSTM1',
    'EGFR',
    'PRADC1',
    'CD34',
    'ITGA3',
    'ITGBA',
    'VEGFA',
    'GIT1'
] AS RISK_FACTORS_LIST
unwind RISK_FACTORS_LIST as RISK_FACTORS_UNWIND
with toLower(RISK_FACTORS_UNWIND) as RISK_FACTORS_LOWERCASE
with collect(RISK_FACTORS_LOWERCASE) as RISK_FACTORS_LOWERCASE_LIST
match result = SHORTESTPATH((risk_fact)-[r*..7]->(n:Disease {name: "oral cavity cancer"}))
where risk_fact.name in RISK_FACTORS_LOWERCASE_LIST
RETURN n, RISK_FACTORS_LOWERCASE_LIST, result

<!-- 'all' risk factors from literatures mappings  -->
WITH [
    'tobacco tar',
    'tobacco smoke pollution',
    "Exacerbated by tobacco use",

    'smoke',
    'ethanol',
    'allyl alcohol',

    "anogenital human papillomavirus infection",
    'human papilloma virus infection',
    'human papillomavirus-related squamous cell carcinoma',
    'human papillomavirus-related penile squamous cell carcinoma',
    'Persistent human papillomavirus infection',

    'human herpesvirus 8-related tumor',
    
    'herpes simplex virus gingivostomatitis',
    'herpes simplex virus keratitis',
    'Renal tubular herpes simplex virus inclusions',

    'syphilis',

    'candida glabrata',
    'candidal paronychia',
    'Recurrent candida infections',
    'Candida esophagitis',

    'periodontitis',

    'mucocutaneous ulceration, chronic',

    'bacterial infectious disease with sepsis',

    'oropharyngeal carcinoma',
    'nasopharyngeal carcinoma',

    'Formaldehyde',
    'Increased cellular sensitivity to UV light',

    'Sulfur Dioxide',
    'Asbestos',
    'Pesticides',
    'Transport of inorganic cations/anions and amino acids/oligopeptides',

    'malignant tumor of floor of mouth',

    'TP53',
    'GSTM1',
    'EGFR',
    'PRADC1',

    'CD34',
    'ITGA3',
    'ITGBA',
    'VEGFA',
    'GIT1'
] AS RISK_FACTORS_LIST
unwind RISK_FACTORS_LIST as RISK_FACTORS_UNWIND
with toLower(trim(RISK_FACTORS_UNWIND)) as RISK_FACTORS_LOWERCASE
with collect(RISK_FACTORS_LOWERCASE) as RISK_FACTORS_LOWERCASE_LIST
// match result = SHORTESTPATH((risk_fact)-[r*..10]->(n:Disease {name: "oral cavity cancer"}))
match result = allshortestpaths((risk_fact)-[r*..10]->(n:Disease {name: "oral cavity cancer"}))
where risk_fact.name in RISK_FACTORS_LOWERCASE_LIST
RETURN n, RISK_FACTORS_LOWERCASE_LIST, result
